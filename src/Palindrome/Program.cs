﻿using System;

namespace Palindrome
{
    public class Program
    {
        private static void Main()
        {
            Console.WriteLine(IsPalindrome("racecar"));
        }

        public static bool IsPalindrome(string word)
        {
            if (string.IsNullOrEmpty(word)) throw new ArgumentException();

            word = word.ToLower();

            var first = word.Substring(0, word.Length / 2);
            var arr = word.ToCharArray();

            Array.Reverse(arr);

            var temp = new string(arr);
            var second = temp.Substring(0, temp.Length / 2);

            return first.Equals(second);
        }
    }
}
