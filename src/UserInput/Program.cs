﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UserInput
{
    public class Program
    {
        private static void Main()
        {
            TextInput input = new NumericInput();
            input.Add('1');
            input.Add('a');
            input.Add('0');

            Console.WriteLine(input.GetValue());
        }
    }

    public class TextInput
    {
        public IList<char> list = new List<char>();

        public virtual void Add(char c)
        {
            list.Add(c);
        }

        public string GetValue()
        {
            return new string(list.ToArray());
        }
    }

    public class NumericInput : TextInput
    {
        public override void Add(char c)
        {
            if (char.IsDigit(c))
                list.Add(c);

        }
    }
}
